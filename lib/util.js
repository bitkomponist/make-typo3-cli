const {exec,spawn} = require('child_process');
const {readFileSync,existsSync} = require('fs');
const {join} = require('path');
const handleData = data => console.log(data.toString());
const handleError = data => console.warn(data.toString());
module.exports.exec = async (script, onData = handleData, onError = handleError, options = {maxBuffer:1024 * 1024 * 5}) => {
    return new Promise((res,rej) => {
        const proc = exec(script,options);

        let data = '';
        proc.stdout.on('data', (chunk)=>{
            data += chunk.toString();
            onData && onData(chunk);
        });
        
        proc.stderr.on('data', onError);
        
        proc.on('close', function (code){
            if(!code) {
                res(data);
            }else{
                rej(code);
            }
        });
    });
}

module.exports.spawn = async (cmd,args = [], onData = handleData, onError = handleError, options = {maxBuffer:1024 * 1024 * 5}) => {
    return new Promise((res,rej) => {
        const proc = spawn(cmd,args,options);

        let data = '';
        proc.stdout.on('data', (chunk)=>{
            data += chunk.toString();
            onData(chunk);
        });
        
        proc.stderr.on('data', onError);
        
        proc.on('close', function (code){
            if(!code) {
                res(data);
            }else{
                rej(code);
            }
        });
    });
}

const envFile = join(process.cwd(),'.project/.env');

const getEnv = (key,def=undefined) => {
    
    function parse (src) {
        const obj = {}
      
        // convert Buffers before splitting into lines and processing
        src.toString().split('\n').forEach(function (line) {
          // matching "KEY' and 'VAL' in 'KEY=VAL'
          const keyValueArr = line.match(/^\s*([\w.-]+)\s*=\s*(.*)?\s*$/)
          // matched?
          if (keyValueArr != null) {
            const key = keyValueArr[1]
      
            // default undefined or missing values to empty string
            let value = keyValueArr[2] || ''
      
            // expand newlines in quoted values
            const len = value ? value.length : 0
            if (len > 0 && value.charAt(0) === '"' && value.charAt(len - 1) === '"') {
              value = value.replace(/\\n/gm, '\n')
            }
      
            // remove any surrounding quotes and extra spaces
            value = value.replace(/(^['"]|['"]$)/g, '').trim()
      
            obj[key] = value
          }
        })
      
        return obj
    }
    
    if(getEnv.cache === undefined) {
        if(!existsSync(envFile)) return def;

        getEnv.cache = parse(readFileSync(envFile,{encoding:'utf8'}));
    }

    if(!(key in getEnv.cache)) return def;

    return getEnv.cache[key];
}

module.exports.getEnv = getEnv;

module.exports.ensurePhp = async (major = 7,minor = 2) => {
    if((await module.exports.exec(`which php`,null)).endsWith('not found')) {
        throw new Error('php not installed');
    }
    
    const [installedMajor,installedMinor] = (await module.exports.exec(`php -v`,null)).split(' ')[1].split('.').map(parseInt);

    if(major < installedMajor || minor < installedMinor){
        throw new Error(`php version must be at least ${major}.${minor}`);
    }
}

module.exports.ensureComposer = async () => {
    if(!existsSync(join(process.cwd(),'app/vendor/bin/composer'))) {
        throw new Error('composer not installed');
    }
}

module.exports.installComposer = async () => {
    await module.exports.ensurePhp();
    const installDir = join(process.cwd(),'app/vendor/bin');
    if(!existsSync(installDir)) {
        await module.exports.exec(`mkdir -p "${installDir}"`);
    }

    await module.exports.exec(`curl -sS https://getcomposer.org/installer | php -- --install-dir="${installDir}" --filename=composer`)
}