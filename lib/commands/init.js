const up = require('./up');
const {existsSync,readdirSync} = require('fs');
const {join} = require('path');
const {exec,ensurePhp} = require('../util');
const composer = require('./composer');
const importDb = require('./import');

module.exports = async ([version='^8']) => {
    const projDir = join(process.cwd(),'.project');

    if(!existsSync(projDir)) {
        //todo clone repo..
        throw new Error('not a make-typo3 repository');
    }

    await ensurePhp();

    const dotEnvFile = join(projDir,'.env')

    if(!existsSync(dotEnvFile)) {
        // init .env file
        console.log('no .env file found, creating one from template');
        
        const tpl = join(projDir,'.env.template');
        if(!existsSync(tpl)){
            throw new Error('.env.template not found, please create one yourself and re-init');
        }
        await exec(`cp ${tpl} ${dotEnvFile}`);
    }

    // build and start containers
    await up(['-s']);

    const appDir = join(process.cwd(),'app');

    const composerFile = join(appDir,'composer.json');

    if(existsSync(composerFile)){
        console.log('project already created, installing dependencies');
        await composer(['install']);
        
        if(existsSync(importDb.dbFile)) {
            console.log('found dump, importing');
            await importDb();
        }

    }else if(existsSync(appDir) && readdirSync(appDir).length){
        throw new Error('app directory must be empty');
    }else {
        // init composer project
        await composer(['create-project','typo3/cms-base-distribution','app',version],'./');

        // create FIRST_INSTALL file
        await exec(`touch ./app/public/FIRST_INSTALL`);
    }

    // open browser
    await exec(`open http://localhost`);
}

module.exports.help = `initializes a fresh typo3 project, currently requiring the .project folder structure to be present`;