const exec = require('./exec');

module.exports = async (args) => {
    return exec(['../vendor/bin/typo3cms',...args],'/var/www/html/app/public');
}

module.exports.help = `executes typo3-cli commands on the web container`;