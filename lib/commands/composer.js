const {spawn,ensurePhp,ensureComposer,installComposer} = require('../util');
const {join} = require('path');
const handleData = data => console.log(data.toString());
const handleError = data => console.warn(data.toString());

module.exports = async (args=[],dir='app') => {
    await ensurePhp();
    try{
        await ensureComposer();
    }catch(e){
        await installComposer();
    }
    await spawn('php',['./vendor/bin/composer',...args],handleData,handleError,{maxBuffer:1024 * 1024 * 5,cwd:join(process.cwd(),dir)});
}

module.exports.help = `executes composer commands on the web container`;