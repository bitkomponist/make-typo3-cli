const {exec} = require('../util');

const removeContainer = name => `docker rm $(docker stop $(docker ps -a -q --filter="name=${name}" --format="{{.ID}}"))`;

module.exports = async () => {
    return exec(`
        ${removeContainer('dev_pma')}
        ${removeContainer('dev_web')}
        ${removeContainer('dev_db')}
    `);
}

module.exports.help = `shuts down and removes all project containers (dev_pma,dev_web,dev_db)`;