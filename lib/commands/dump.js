const {exec,getEnv} = require('../util');
const {join} = require('path');
const wd = process.cwd();

const dbFile = join(wd,'.project/database.sql');

module.exports = async () => {
    const [host,user,password,database] = ['MYSQL_HOST','MYSQL_USER','MYSQL_PASSWORD','MYSQL_DATABASE'].map(getEnv);
    return exec(`
    if [ ! "$(docker ps -q -f name=dev_db)" ]; then  
        echo "no container present, exporting from ${host}"
        mysqldump -h ${host} -u ${user} -p${password} ${database} > "${dbFile}"
    else 
        echo "no host present, exporting from docker container";
        if docker ps | grep dev_db > /dev/null; then
            echo "Found database container";
        else
            echo "Database container (dev_db) is not running";
            exit 1;
        fi

        echo "exporting database"
        docker exec dev_db /usr/bin/mysqldump -u ${user} -p${password} ${database} > "${dbFile}"
    fi

    exit 0
    `);
}

module.exports.help = `exports the complete project db to ${dbFile}`;