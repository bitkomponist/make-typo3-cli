#!/usr/bin/env node
const {join,basename} = require('path');
const {readdirSync} = require('fs');
module.exports = async (args) => {
    const commandsDir = __dirname;

    readdirSync(commandsDir).filter(filename => filename.match(/.js$/) ).forEach((commandFile) => {
        const module = require(join(commandsDir,commandFile));
        const name = basename(commandFile,'.js');
        const {help} = module;

        if(!help) {
            console.log(`${name}\t\t\tundocumented\n`);
            return;
        }

        console.log(`${name}\n${typeof(help)==='function'?help():help}\n`);
    });
}

module.exports.help = `displays help for all available commands`;