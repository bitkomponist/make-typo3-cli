const {join} = require('path');

module.exports = async (args) => {
    console.log(require(join(__dirname,'../../package.json')).version);
}

module.exports.help = `echos the cli version`;