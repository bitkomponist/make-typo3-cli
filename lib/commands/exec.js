const {spawn} = require('../util');
module.exports = async (args,wd='/var/www/html/app') => {
    console.warn('exec is deprecated');
    return spawn('docker',['exec','-u','www-data','-w',wd,'dev_web',...args]);
}

module.exports.help = `executes shell commands on the dev_web container, as user www-data`;