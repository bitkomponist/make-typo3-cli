const {exec,getEnv} = require('../util');
const {join} = require('path');
const wd = process.cwd();

const dbFile = join(wd,'.project/database.sql');

module.exports = async () => {
    const [host,user,password,database] = ['MYSQL_HOST','MYSQL_USER','MYSQL_PASSWORD','MYSQL_DATABASE'].map(getEnv);

    return exec(`
    
    if [ ! "$(docker ps -q -f name=dev_db)" ]; then 
        echo "no container present, importing into ${host}"
        mysql -h ${host} -u ${user} -p${password} ${database} < "${dbFile}"
    else 
        echo "no host present, importing into docker container";
        if docker ps | grep dev_db > /dev/null; then
            echo "Found database container";
        else
            echo "Database container (dev_db) is not running";
            exit 1;
        fi
        cat "${dbFile}" | docker exec -i dev_db /usr/bin/mysql -h db -u ${user} -p${password} ${database};
    fi

    exit 0
    `);
}

module.exports.dbFile = dbFile;

module.exports.help = `imports the complete project into the mysql container, using the sql dump located at ${dbFile}`;