const {spawn,getEnv} = require('../util');
const {join} = require('path');
const wd = process.cwd();

const resolverScript = join(wd,'app/public/typo3conf/ext/make/Build/build');

module.exports = async (args) => {
    return spawn(resolverScript,args);
}

module.exports.help = `execute npm commands on client packages, usage: client <which> [...npm args]`;