const {exec} = require('../util');
const {join} = require('path');
const down = require('./down');
const wd = process.cwd();

const dockerFile = join(wd,'.project/docker-compose.yml')

module.exports = async (args = [], containerList = '') => {
    try {
        await down();
    }catch(e){}

    const silent = args.includes('-s');
    
    await exec(`docker-compose -f "${dockerFile}" build`);
    
    await exec(`docker-compose -f "${dockerFile}" up -d ${containerList}`);

    if(!silent){
        await exec(`docker logs -f dev_web`);
    }
}

module.exports.help = `boots all project containers (dev_pma,dev_web,dev_db)`;