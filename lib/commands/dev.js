const {exec,ensurePhp} = require('../util');
const up = require('./up');

module.exports = async (args = []) => {
    await ensurePhp();

    try {
        await down();
    }catch(e){}

    exec(`cd ./app/public && TYPO3_CONTEXT=Development php -d variables_order=EGPCS -S 127.0.0.1:80`);
    up(['-s']);
}

module.exports.help = `starts local dev server`;