#!/usr/bin/env node
const {join} = require('path');
const {existsSync} = require('fs');
const [,,command='init',...args] = process.argv;
const commandModule = join(__dirname,'commands',command+'.js');
if(!existsSync(commandModule)) throw new Error(`${command} is not a command`);
require(commandModule)(args);